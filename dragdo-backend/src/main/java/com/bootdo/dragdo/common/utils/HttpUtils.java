package com.bootdo.dragdo.common.utils;

import com.alibaba.fastjson.JSONObject;
import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;

public class HttpUtils {
    public static String getString(String url) {
        OkHttpClient okHttpClient = new OkHttpClient();
        final Request request = new Request.Builder()
                .url(url)
                .build();
        final Call call = okHttpClient.newCall(request);
        try {
            Response response =  call.execute();
            return response.body().string();
        } catch (IOException e) {
            throw new RuntimeException("http异常");
        }
    }
}
