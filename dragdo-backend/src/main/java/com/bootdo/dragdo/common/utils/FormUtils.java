package com.bootdo.dragdo.common.utils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.bootdo.dragdo.domain.FieldDTO;
import com.bootdo.dragdo.domain.FormDTO;
import com.fasterxml.jackson.databind.util.JSONPObject;

import java.util.ArrayList;
import java.util.List;

public class FormUtils {
    public static List<FieldDTO> transform(FormDTO formDTO) {
        JSONObject jsonObject = JSONObject.parseObject(formDTO.getJson());
        List<FieldDTO> fieldDTOS = new ArrayList<>();
        return listFields(fieldDTOS,jsonObject);
    }

    static List<FieldDTO> listFields(List<FieldDTO> list,JSONObject jsonObject){
        Boolean isList = jsonObject.containsKey("list")||
                jsonObject.containsKey("columns");
        if(isList){
           //区段或栅格
            List<JSONObject> jsonObjects =  tans(jsonObject);
            for (JSONObject object : jsonObjects) {
                listFields(list,object);
            }
        }else {
            //表单元素
            FieldDTO fieldDTO = jsonObject.toJavaObject(FieldDTO.class);
            list.add(fieldDTO);
        }
        return list;
    }

   static List<JSONObject> tans(JSONObject jsonObject){
       List<JSONObject> l0 = new ArrayList<>();
       if(jsonObject.containsKey("list")){
          l0 = jsonObject.getJSONArray("list").toJavaList(JSONObject.class);
        }
       if(jsonObject.containsKey("columns")) {
           List<JSONObject> l1 = jsonObject.getJSONArray("columns").toJavaList(JSONObject.class);
           l0.addAll(l1);
       }
        return l0;
    }
}
