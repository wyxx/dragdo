package com.bootdo.dragdo.common.utils;

import com.bootdo.dragdo.domain.ColumnDO;
import com.bootdo.dragdo.domain.FieldDTO;
import com.bootdo.dragdo.domain.FormDTO;
import com.bootdo.dragdo.domain.TableDO;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * 代码生成器   工具类
 */
public class GenUtils {


    public static List<String> getTemplates(String projectTemplate) {
        List<String> templates = new ArrayList<String>();
        templates.add("templates/"+projectTemplate+"/domain.java.vm");
        templates.add("templates/"+projectTemplate+"/Dao.java.vm");
        //templates.add("templates/"+projectTemplate+"/Mapper.java.vm");
        templates.add("templates/"+projectTemplate+"/Mapper.xml.vm");
        templates.add("templates/"+projectTemplate+"/Service.java.vm");
        templates.add("templates/"+projectTemplate+"/ServiceImpl.java.vm");
        templates.add("templates/"+projectTemplate+"/Controller.java.vm");
        templates.add("templates/"+projectTemplate+"/menu.sql.vm");
        return templates;
    }

    /**
     * 生成代码
     */

    public static void generatorCode(FormDTO formDTO, ZipOutputStream zip) {
        //配置信息
        Configuration config = getConfig();
        //表信息
        TableDO tableDO = new TableDO();
        tableDO.setTableName(formDTO.getName());
        tableDO.setComments(formDTO.getNote());
        //表名转换成Java类名
        String className = tableToJava(tableDO.getTableName(), config.getString("tablePrefix"), config.getString("autoRemovePre"));
        tableDO.setClassName(className);
        tableDO.setClassname(StringUtils.uncapitalize(className));

        //列信息
        List<ColumnDO> columsList = new ArrayList<>();
        for (FieldDTO fieldDTO : formDTO.getList()) {
            ColumnDO columnDO = new ColumnDO();
            columnDO.setColumnName(fieldDTO.getModel());
            columnDO.setDataType(fieldDTO.getType());
            columnDO.setComments(fieldDTO.getName());
            columnDO.setExtra("");

            //列名转换成Java属性名
            String attrName = columnToJava(columnDO.getColumnName());
            columnDO.setAttrName(attrName);
            columnDO.setAttrname(StringUtils.uncapitalize(attrName));

            //列的数据类型，转换成Java类型
            String attrType = config.getString(columnDO.getDataType(), "unknowType");
            columnDO.setAttrType(attrType);

            //是否主键
//            if ("PRI".equalsIgnoreCase(column.get("columnKey")) && tableDO.getPk() == null) {
//                tableDO.setPk(columnDO);
//            }

            columsList.add(columnDO);
        }
        tableDO.setPk(new ColumnDO() {{
            setColumnName(formDTO.getPk());
            setAttrname(formDTO.getPk());
            setAttrName(formDTO.getPk());
            //TODO
            setAttrType("String");
        }});
        tableDO.setColumns(columsList);

        //没主键，则第一个字段为主键
        if (tableDO.getPk() == null) {
            tableDO.setPk(tableDO.getColumns().get(0));
        }

        //设置velocity资源加载器
        Properties prop = new Properties();
        prop.put("file.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        Velocity.init(prop);

        //封装模板数据
        Map<String, Object> map = new HashMap<>(16);
        map.put("tableName", tableDO.getTableName());
        map.put("comments", formDTO.getNote());
        map.put("pk", tableDO.getPk());
        map.put("className", tableDO.getClassName());
        map.put("classname", tableDO.getClassname());
        map.put("pathName", config.getString("package").substring(config.getString("package").lastIndexOf(".") + 1));
        map.put("columns", tableDO.getColumns());
        map.put("fields", formDTO.getFields());
        map.put("package", config.getString("package"));
        map.put("author", formDTO.getAuthor());
        map.put("email", config.getString("email"));
        map.put("datetime", DateUtils.format(new Date(), DateUtils.DATE_TIME_PATTERN));
        VelocityContext context = new VelocityContext(map);

        //获取模板列表
        List<String> templates = getTemplates(formDTO.getProjectTemplate());
        for (String template : templates) {
            //渲染模板
            StringWriter sw = new StringWriter();
            Template tpl = Velocity.getTemplate(template, "UTF-8");
            tpl.merge(context, sw);

            try {
                //添加到zip
                zip.putNextEntry(new ZipEntry(getFileName(template, tableDO.getClassname(), tableDO.getClassName(), config.getString("package").substring(config.getString("package").lastIndexOf(".") + 1))));
                IOUtils.write(sw.toString(), zip, "UTF-8");
                IOUtils.closeQuietly(sw);
                zip.closeEntry();
            } catch (IOException e) {
                throw new RuntimeException("渲染模板失败，表名：" + tableDO.getTableName(), e);
            }
        }
    }

    public static Map generatorCode(FormDTO formDTO) {
        //配置信息
        Configuration config = getConfig();
        //表信息
        List<FieldDTO> fieldDTOS = FormUtils.transform(formDTO);

        FieldDTO pk = new FieldDTO() {{
            setDbType("varchar(100)");
            setModel("id");
            setJavaType("String");
            setName("id");
            setColumn("id");
        }};
        fieldDTOS.add(0, pk );
        fieldDTOS.forEach(fieldDTO -> {
            fieldDTO.setColumn(com.bootdo.dragdo.common.utils.StringUtils.camelToLine(
                    fieldDTO.getModel()
            ));
            if("varchar".equals(fieldDTO.getDbType())){
                fieldDTO.setDbType("varchar("+fieldDTO.getOptions().getSize()+")");
            }
        });


        //设置velocity资源加载器
        Properties prop = new Properties();
        prop.put("file.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        Velocity.init(prop);

        String tableName = formDTO.getTablePrefix()+ com.bootdo.dragdo.common.utils.StringUtils
                .camelToLine(formDTO.getName());

        //封装模板数据
        Map<String, Object> map = new HashMap<>(16);
        map.put("tableName", tableName);
        map.put("comments", formDTO.getNote());
        map.put("pk", pk);
        map.put("className", StringUtils.capitalize(formDTO.getName()));
        map.put("classname", formDTO.getName());
        map.put("pathName", config.getString("package").substring(config.getString("package").lastIndexOf(".") + 1));
        map.put("columns", formDTO.getList());
        map.put("fields", fieldDTOS);
        map.put("package", formDTO.getPackagePath());
        map.put("author", formDTO.getAuthor());
        map.put("email", config.getString("email"));
        map.put("datetime", DateUtils.format(new Date(), DateUtils.DATE_TIME_PATTERN));
        map.put("useLombok", formDTO.getUseLombok());
        VelocityContext context = new VelocityContext(map);

        //获取模板列表
        List<String> templates = getTemplates(formDTO.getProjectTemplate());
        Map map1 = new HashMap();
        for (String template : templates) {
            //渲染模板
            StringWriter sw = new StringWriter();
            Template tpl = Velocity.getTemplate(template, "UTF-8");
            tpl.merge(context, sw);
            template = template.substring(0, template.indexOf("."));
            map1.put(template.replace("templates/"+formDTO.getProjectTemplate() +"/", ""), sw.getBuffer());
        }
        return map1;
    }


    /**
     * 列名转换成Java属性名
     */
    public static String columnToJava(String columnName) {
        return WordUtils.capitalizeFully(columnName, new char[]{'_'}).replace("_", "");
    }

    /**
     * 表名转换成Java类名
     */
    public static String tableToJava(String tableName, String tablePrefix, String autoRemovePre) {
//        if (Constant.AUTO_REOMVE_PRE.equals(autoRemovePre)) {
        //TODO 自动去除表前缀
        if (true) {
            tableName = tableName.substring(tableName.indexOf("_") + 1);
        }
        if (StringUtils.isNotBlank(tablePrefix)) {
            tableName = tableName.replace(tablePrefix, "");
        }

        return columnToJava(tableName);
    }

    /**
     * 获取配置信息
     */
    public static Configuration getConfig() {
        try {
            return new PropertiesConfiguration("generator.properties");
        } catch (ConfigurationException e) {
            throw new RuntimeException("获取配置文件失败，", e);
        }
    }

    /**
     * 获取文件名
     */
    public static String getFileName(String template, String classname, String className, String packageName) {
        String packagePath = "main" + File.separator + "java" + File.separator;
        //String modulesname=config.getString("packageName");
        if (StringUtils.isNotBlank(packageName)) {
            packagePath += packageName.replace(".", File.separator) + File.separator;
        }

        if (template.contains("domain.java.vm")) {
            return packagePath + "domain" + File.separator + className + "DO.java";
        }

        if (template.contains("Dao.java.vm")) {
            return packagePath + "dao" + File.separator + className + "Dao.java";
        }

//		if(template.contains("Mapper.java.vm")){
//			return packagePath + "dao" + File.separator + className + "Mapper.java";
//		}

        if (template.contains("Service.java.vm")) {
            return packagePath + "service" + File.separator + className + "Service.java";
        }

        if (template.contains("ServiceImpl.java.vm")) {
            return packagePath + "service" + File.separator + "impl" + File.separator + className + "ServiceImpl.java";
        }

        if (template.contains("Controller.java.vm")) {
            return packagePath + "controller" + File.separator + className + "Controller.java";
        }

        if (template.contains("Mapper.xml.vm")) {
            return "main" + File.separator + "resources" + File.separator + "mapper" + File.separator + packageName + File.separator + className + "Mapper.xml";
        }


        if (template.contains("menu.sql.vm")) {
            return className.toLowerCase() + "_menu.sql";
        }

        return null;
    }
}
