package com.bootdo.dragdo.service;

import com.bootdo.dragdo.domain.FormDTO;

import java.util.Map;

public interface GeneratorService {
    Map generatorBean(FormDTO formDTO);
    FormDTO saveForm( FormDTO formDTO);
}
