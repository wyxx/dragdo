package com.bootdo.dragdo.service.impl;

import com.bootdo.dragdo.domain.FormDTO;
import com.bootdo.dragdo.mapper.FormMapper;
import com.bootdo.dragdo.service.FormService;
import com.fasterxml.jackson.databind.util.JSONPObject;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class FormServiceImpl implements FormService {
    @Autowired
    FormMapper formMapper;

    @Override
    public FormDTO get(String id) {
        return formMapper.get(id);
    }

    @Override
    public String saveForm(FormDTO formDTO) {
        if (StringUtils.isBlank(formDTO.getId())) {
            formDTO.setId(UUID.randomUUID().toString().replace("-", ""));
            formMapper.saveForm(formDTO);
        } else {
            formMapper.update(formDTO);
        }
        return formDTO.getId();
    }

    @Override
    public List list(String key) {
        return formMapper.list(key);
    }

    @Override
    public Integer remove(String id ){
        return formMapper.remove(id);
    }
}
