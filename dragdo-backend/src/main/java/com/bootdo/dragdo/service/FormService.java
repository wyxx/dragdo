package com.bootdo.dragdo.service;

import com.bootdo.dragdo.domain.FormDTO;

import java.util.List;

public interface FormService {

    FormDTO get(String id);

    String saveForm(FormDTO formDTO);

    List list(String key);

    Integer remove(String id);
}
