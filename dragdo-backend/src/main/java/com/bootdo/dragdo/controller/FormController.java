package com.bootdo.dragdo.controller;

import com.bootdo.dragdo.domain.FormDTO;
import com.bootdo.dragdo.service.FormService;
import org.apache.ibatis.annotations.Delete;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(("/form"))
public class FormController {
    @Autowired
    FormService formService;

    @GetMapping("/{id}")
    FormDTO get(@PathVariable("id") String id) {
        return formService.get(id);
    }

    @GetMapping
    List list(String key) {
        return formService.list(key);
    }

    @PostMapping("/save")
    String saveForm(@RequestBody FormDTO formDTO) {
        return formService.saveForm(formDTO);
    }

    @DeleteMapping
    Integer delete(String id) {
        return formService.remove(id);
    }


}
