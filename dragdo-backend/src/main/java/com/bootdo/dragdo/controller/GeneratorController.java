package com.bootdo.dragdo.controller;

import com.alibaba.fastjson.JSONObject;
import com.bootdo.dragdo.common.utils.FormUtils;
import com.bootdo.dragdo.common.utils.HttpUtils;
import com.bootdo.dragdo.common.utils.StringUtils;
import com.bootdo.dragdo.domain.FieldDTO;
import com.bootdo.dragdo.domain.FormDTO;
import com.bootdo.dragdo.service.GeneratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

@RequestMapping("/generator")
@RestController
public class GeneratorController {
    @Autowired
    GeneratorService generatorService;

    @PostMapping("/generatorBean")
    Map generatorBean(HttpServletResponse response, @RequestBody FormDTO formDTO) {
        return generatorService.generatorBean(formDTO);
    }

    @PostMapping("/transform")
    public  List<FieldDTO> transform(@RequestBody FormDTO formDTO){
        return FormUtils.transform(formDTO);
    }

    @GetMapping("translate")
    String translate(String source){
        String url = "http://fanyi.youdao.com/translate?&doctype=json&type=AUTO&i="+source;
        String resStr = HttpUtils.getString(url);
        JSONObject jsonObject = JSONObject.parseObject(resStr);
        String res =  jsonObject.getJSONArray("translateResult").
                getJSONArray(0).getJSONObject(0).getString("tgt");
        res = res.replace("The ","");
        return StringUtils.spaceToCamel(res,true);
    }
}
