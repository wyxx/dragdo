package com.bootdo.dragdo.domain;

import lombok.Data;

@Data
public class FieldDTO {
    private String model;
    private String name;
    private Integer size;
    private String column;
    private String type;
    private String javaType;
    private String dbType;
    private Options options;

    @Data
    public static class Options {
        Integer size;
    }
}
