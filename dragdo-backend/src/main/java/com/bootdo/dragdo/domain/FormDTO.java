package com.bootdo.dragdo.domain;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.databind.util.JSONPObject;
import lombok.Data;
import lombok.ToString;

import java.util.List;

@ToString
@Data
public class FormDTO {
    private String id;
    private String packagePath;
    private String projectTemplate;
    private String name;
    private String note;
    private String author;
    private String size;
    private String labelPosition;
    private Integer labelWidth;
    private String tablePrefix;
    private List<FieldDTO> list;
    private String fields;
    private Boolean useLombok;
    private String pk;

    private String json;
}
