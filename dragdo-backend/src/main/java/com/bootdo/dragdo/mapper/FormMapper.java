package com.bootdo.dragdo.mapper;

import com.bootdo.dragdo.domain.FormDTO;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface FormMapper {

    @Select("select * from t_form where id =#{id}")
    FormDTO get(String id);

    @Insert("insert t_form(id,name,note,json) values(#{id},#{name},#{note},#{json})")
    Integer saveForm(FormDTO formDTO);

    @Update("update t_form set name = #{name},note=#{note},json=#{json} where id=#{id}")
    Integer update(FormDTO formDTO);

    @Select("select * from t_form")
    List<FormDTO> list(String key);

    @Delete("delete from t_form where id =#{id}")
    Integer remove(String id);
}
