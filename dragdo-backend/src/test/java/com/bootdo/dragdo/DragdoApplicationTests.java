package com.bootdo.dragdo;

import com.bootdo.dragdo.common.utils.FormUtils;
import com.bootdo.dragdo.common.utils.StringUtils;
import com.bootdo.dragdo.domain.FieldDTO;
import com.bootdo.dragdo.domain.FormDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DragdoApplicationTests {


    @Test
    public void contextLoads() {

    }

    public static void main(String[] args) {
        String str = "ASSET TYPE CODE";
        System.out.println(
                StringUtils.spaceToCamel(str,false)
        );
    }
}
