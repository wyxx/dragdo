import Vue from 'vue'
import Router from 'vue-router'
import Index from '@/components/Index'
import Demo from '@/components/Demo'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Index',
      component: Index,
      children: [
        {
          path: '/demo',
          name: 'Demo',
          component: Demo,
        },
      ]
    }
  ]
})
