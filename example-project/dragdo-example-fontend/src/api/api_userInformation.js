/**
 * Created by bootdo.
 * 表单相关api
 */
import * as API from './'

export default {
  get:id=>{
    return API.GET('/userInformation/'+id,{})
  },
  list: params => {
    return API.GET('/userInformation', params)
  },
  save: params => {
    return API.POST('/userInformation', params)
  },
  update: params => {
    return API.PUT('/userInformation', params)
  },

  remove:params=>{
    return API.DELETE('/userInformation',params)
  }
}
