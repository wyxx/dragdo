package com.bootdo.dragdo.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;


/**
 * 用户信息
 *
 * @author lichunguang
 * @email 1992lcg@163.com
 * @date 2020-02-12 15:17:05
 */
@Data
public class UserInformationDO implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
    * id
    */
    private String id;
    /**
    * 姓名
    */
    private String name;
    /**
    * 出生日期
    */
    private Date dateOfBirth;
    /**
    * 性别
    */
    private String gender;
    /**
    * 简介
    */
    private String introductionToThe;
}
