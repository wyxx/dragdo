package com.bootdo.dragdo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.bootdo.dragdo.dao.UserInformationDao;
import com.bootdo.dragdo.domain.UserInformationDO;
import com.bootdo.dragdo.service.UserInformationService;



@Service
public class UserInformationServiceImpl implements UserInformationService {
	@Autowired
	private UserInformationDao userInformationDao;
	
	@Override
	public UserInformationDO get(String id){
		return userInformationDao.get(id);
	}
	
	@Override
	public List<UserInformationDO> list(Map<String, Object> map){
		return userInformationDao.list(map);
	}
	
	@Override
	public int count(Map<String, Object> map){
		return userInformationDao.count(map);
	}
	
	@Override
	public int save(UserInformationDO userInformation){
		userInformation.setId(UUID.randomUUID().toString());
		return userInformationDao.save(userInformation);
	}
	
	@Override
	public int update(UserInformationDO userInformation){
		return userInformationDao.update(userInformation);
	}
	
	@Override
	public int remove(String id){
		return userInformationDao.remove(id);
	}
	
	@Override
	public int batchRemove(String[] ids){
		return userInformationDao.batchRemove(ids);
	}
	
}
