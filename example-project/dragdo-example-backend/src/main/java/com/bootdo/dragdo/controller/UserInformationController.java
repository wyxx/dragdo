package com.bootdo.dragdo.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import com.bootdo.dragdo.domain.UserInformationDO;
import com.bootdo.dragdo.service.UserInformationService;

/**
 * 用户信息
 *
 * @author lichunguang
 * @email 1992lcg@163.com
 * @date 2020-02-12 18:03:50
 */

@RestController
@RequestMapping("/userInformation")
public class UserInformationController {
	@Autowired
	private UserInformationService userInformationService;

	@ResponseBody
	@GetMapping()
	public List list(@RequestParam Map<String, Object> params){
		//查询列表数据
		List<UserInformationDO> userInformationList = userInformationService.list(params);
		return userInformationList;
	}

	/**
	 * 保存
	 */
	@PostMapping()
	public String save(@RequestBody UserInformationDO userInformation){
		userInformationService.save(userInformation);
		return "success";
	}
	/**
	 * 修改
	 */
	@PutMapping()
	public String update(@RequestBody UserInformationDO userInformation){
		userInformationService.update(userInformation);
		return "success";
	}

	/**
	 * 删除
	 */
	@DeleteMapping()
	public String remove( String id){
		userInformationService.remove(id);
		return "success";
	}

	/**
	 * 删除
	 */
	@DeleteMapping( "/batchRemove")
	public String remove(@RequestParam("ids[]") String[] ids){
		userInformationService.batchRemove(ids);
		return "success";
	}

}
