package com.bootdo.dragdo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DragDoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DragDoApplication.class, args);
    }

}
