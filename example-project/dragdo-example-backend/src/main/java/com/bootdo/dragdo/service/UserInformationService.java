package com.bootdo.dragdo.service;

import com.bootdo.dragdo.domain.UserInformationDO;

import java.util.List;
import java.util.Map;

/**
 * 用户信息
 * 
 * @author lichunguang
 * @email 1992lcg@163.com
 * @date 2020-02-12 15:17:05
 */
public interface UserInformationService {
	
	UserInformationDO get(String id);
	
	List<UserInformationDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(UserInformationDO userinformation);
	
	int update(UserInformationDO userinformation);
	
	int remove(String id);
	
	int batchRemove(String[] ids);
}
