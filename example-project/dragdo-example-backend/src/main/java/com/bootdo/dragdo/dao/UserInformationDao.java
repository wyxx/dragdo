package com.bootdo.dragdo.dao;

import com.bootdo.dragdo.domain.UserInformationDO;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

/**
 * 用户信息
 * @author lichunguang
 * @email 1992lcg@163.com
 * @date 2020-02-12 15:46:22
 */
@Mapper
public interface UserInformationDao {

	UserInformationDO get(String id);
	
	List<UserInformationDO> list(Map<String,Object> map);
	
	int count(Map<String,Object> map);
	
	int save(UserInformationDO userInformation);
	
	int update(UserInformationDO userInformation);
	
	int remove(String id);
	
	int batchRemove(String[] ids);
}
