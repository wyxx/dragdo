import ApiGenerator from '../api/api_generator'

let opinions = ''

function findRemoteFunc(list, funcList, tokenFuncList, blankList) {
  for (let i = 0; i < list.length; i++) {
    if (list[i].type == 'grid') {
      list[i].columns.forEach(item => {
        findRemoteFunc(item.list, funcList, tokenFuncList, blankList)
      })
    } else {
      if (list[i].type == 'blank') {
        if (list[i].model) {
          blankList.push({
            name: list[i].model,
            label: list[i].name
          })
        }
      } else if (list[i].type == 'imgupload') {
        if (list[i].options.tokenFunc) {
          tokenFuncList.push({
            func: list[i].options.tokenFunc,
            label: list[i].name,
            model: list[i].model
          })
        }
      } else {
        if (list[i].options.remote && list[i].options.remoteFunc) {
          funcList.push({
            func: list[i].options.remoteFunc,
            label: list[i].name,
            model: list[i].model
          })
        }
      }
    }
  }
}

function getItem(item) {
  let html = ''
  if (item.type === 'input') {
    html = `
          <el-input style="width:${item.options.width}" placeholder="${item.options.placeholder}"  :disabled="${item.options.disabled}"
          v-model="model.${item.model}" ></el-input>
     `
  } else if (item.type === 'textarea') {
    html = `
          <el-input type="textarea"  v-model="model.${item.model}" placeholder="${item.options.placeholder}"  :rows="5"></el-input>
     `
  } else if (item.type === 'number') {
    html = `
          <el-input-number v-model="model.${item.model}" ></el-input-number>
     `
  } else if (item.type === 'radio') {
    html = `
          <el-radio-group v-model="model.${item.model}"  :disabled="${item.options.disabled}" >
          <el-radio :label="item.value" v-for="(item, index) in ${item.model}Options">${item.options.showLabel ? '{{item.label}}' : '{{item.value}}'}
</el-radio>
</el-radio-group>
     `
    let opt = JSON.stringify(item.options.options)
    opinions += `
      ${item.model}Options : ${opt},
      `
  } else if (item.type === 'checkbox') {
    html = `
          <el-checkbox-group v-model="model.${item.model}" >
          <el-checkbox :label="item.value" v-for="(item, index) in ${item.model}Options">{{item.label}}</el-checkbox>
</el-checkbox-group>
     `
    let opt = JSON.stringify(item.options.options)
    opinions += `
      ${item.model}Options : ${opt},
      `
  } else if (item.type === 'time') {
    html = `
          <el-time-picker v-model="model.${item.model}" >
</el-time-picker>
    `
  } else if (item.type === 'date') {
    html = `
          <el-date-picker v-model="model.${item.model}"  :readonly="${item.options.readonly}"
        :disabled="${item.options.disabled}"
        :editable="${item.options.editable}"
        :clearable="${item.options.clearable}"
        value-format="'timestamp'"
        format="${item.options.format}">
</el-date-picker>
    `
  } else if (item.type === 'rate') {
    html = `
          <el-rate v-model="model.${item.model}"  >
</el-rate>
    `
  } else if (item.type === 'color') {
    html = `
          <el-color-picker v-model="model.${item.model}" >
</el-color-picker>
    `
  } else if (item.type === 'select') {
    html = `
          <el-select v-model="model.${item.model}" >
          <el-option :value="item.value" v-for="(item, index) in ${item.model}Options">{{item.label}}</el-option>
</el-select>
     `
    let opt = JSON.stringify(item.options.options)
    opinions += `
      ${item.model}Options : ${opt},
      `
  } else if (item.type === 'switch') {
    html = `
          <el-switch v-model="model.${item.model}" >
</el-switch>
    `
  } else if (item.type === 'slider') {
    html = `
          <el-slider v-model="model.${item.model}" >
</el-slider>
    `
  } else {
    html = `
         <el-${item.type} v-model="model.${item.model}"></el-${item.type}> 
     `
  }
  return `<el-form-item label="${item.name}">
          ${html}
     </el-form-item>
     `
}

function getGrid(row) {
  let grids = ''
  for (let col of row.columns) {
    let items = ''
    if (col.list) {
      for (let item of col.list) {
        items += getItem(item)
      }
    }
    grids +=
      `<el-col>
          ${items}
        </el-col>
        `
  }
  grids =
    `<el-row type="flex">
        ${grids}
     </el-row>`
  return grids;
}


export default function (data) {

  const funcList = []

  const tokenFuncList = []

  const blankList = []

  const list = JSON.parse(data).list
  const form = JSON.parse(data)
  let model = ''
  let tabelTemplate = ''
  for (let item of form.model) {
    let init = "''"
    if (item.type === 'checkbox') {
      init = '[]'
    }
    model += item.model + ":" + init + ",\n "
    tabelTemplate += `
     <el-table-column prop="${item.model}" label="${item.name}">
        </el-table-column>
    `
  }
  findRemoteFunc(JSON.parse(data).list, funcList, tokenFuncList, blankList)
  let funcTemplate = ''

  let blankTemplate = ''

  let listTemplate = ''


  let options = ''
  for (let i = 0; i < list.length; i++) {
    if (list[i].type === 'grid') {
      options += list[i].model + 's: ' + JSON.stringify(list[i].options.options) + ','

      listTemplate += getGrid(list[i])
    } else if (list[i].type === 'collapse') {
      var items = ''
      for (let col of list[i].columns) {
        let temps = ''
        for (let item of col.list) {
          let temp = ''
          if (item.type === 'grid') {
            temp += getGrid(item)
          } else {
            temp += getItem(item)
          }
          temps += temp

        }
        items +=
          `<el-collapse-item title="${col.name}">
                ${temps}
             </el-collapse-item>
`
      }
      listTemplate +=
        `<el-collapse>
              ${items}
         </el-collapse>`
    } else {

      listTemplate += getItem(list[i])

    }

  }

  for (let i = 0; i < funcList.length; i++) {
    funcTemplate += `
            ${funcList[i].func} (resolve) {
              // ${funcList[i].label} ${funcList[i].model}
              // 获取到远端数据后执行回调函数
              // resolve(data)
            },
    `
  }

  for (let i = 0; i < tokenFuncList.length; i++) {
    funcTemplate += `
            ${tokenFuncList[i].func} (resolve) {
              // ${tokenFuncList[i].label} ${tokenFuncList[i].model}
              // 获取到token数据后执行回调函数
              // resolve(token)
            },
    `
  }

  for (let i = 0; i < blankList.length; i++) {
    blankTemplate += `
        <template slot="${blankList[i].name}" slot-scope="scope">
          <!-- ${blankList[i].label} -->
          <!-- 通过 v-model="scope.model.${blankList[i].name}" 绑定数据 -->
        </template>
    `
  }
  return `
<template>
  <div>
    <el-header class="bd-row">
      <el-button type="text" @click="dialogFormVisible = true">新建</el-button>
    </el-header>

    <el-main class="bd-row">
      <el-table :data="tableData">
        ${tabelTemplate}
         <el-table-column
          label="操作"
          width="100">
          <template slot-scope="scope">
            <el-button @click="handleClick(scope.row)" type="text" size="small">查看</el-button>
            <el-button @click="remove(scope.row.id)" type="text" size="small">删除</el-button>
          </template>
        </el-table-column>
      </el-table>
      <el-dialog :visible.sync="dialogFormVisible">
        <el-form :label-position=labelPosition  label-width="80px">
          ${listTemplate}
        </el-form>
        <span slot="footer" class="dialog-footer">
            <el-button @click="dialogFormVisible = false">取 消</el-button>
            <el-button type="primary" @click="save">确 定</el-button>
          </span>
      </el-dialog>
    </el-main>
  </div>
</template>

<script>
  import api_${form.name} from '../api/api_${form.name}'

  export default {
    name: "Demo",
    data() {
      return {
        tableData:[],
        dialogFormVisible: false,
        labelPosition: '${form.labelPosition}',
        model:{
            ${model}
        },
        ${opinions}
      }
    },
    methods:{
      list(){
        api_${form.name}.list({}).then(
          res=>{
            this.tableData = res
          }
        )
      },
      save(){
        if(this.model.id){
           api_${form.name}.update(this.model).then(
          res=>{
            this.dialogFormVisible = false
            this.list()
          }
        )
        }else {
          api_${form.name}.save(this.model).then(
          res=>{
            this.dialogFormVisible = false
            this.list()
          }
        )
        }
        
      },
      remove(id){
        this.$confirm('此操作将永久删除该文件, 是否继续?', '提示', {
          confirmButtonText: '确定',
          cancelButtonText: '取消',
          type: 'warning'
        }).then(() => {
          api_${form.name}.remove({id:id}).then(
          res=>{
             this.$message({
            type: 'success',
            message: '删除成功!'
          });
            this.list()
          }
        )
         
        })
        
      },
    },
    mounted() {
      this.list()
    }
  }
</script>

<style scoped>

</style>

`


}
