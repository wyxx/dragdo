/**
 * Created by bootdo.
 * 代码生成相关api
 */
import * as API from './'

export default {


  addUser:params =>{
    return API.POST('/generator/generatorBean',params)
  },
  transform:params =>{
    return API.POST('/generator/transform',params)
  },

  translate:params =>{
    return API.GET('/generator/translate',params)
  }

}
